<?php declare(strict_types=1);

use PhpStyler\Config;
use PhpStyler\Files;
use PhpStyler\Styler;

$paths = (require __DIR__ . '/code-quality-paths.php');

return new Config(
    styler: new Styler,
    files: new Files(...$paths),
    cache: __DIR__ . '/.php-styler.cache',
);
