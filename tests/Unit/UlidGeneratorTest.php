<?php declare(strict_types=1);

use Doctrine\ORM\EntityManager;
use Lubiana\DoctrineUlid\IdGenerator\UlidGenerator;
use Symfony\Component\Uid\Factory\UlidFactory;
use Symfony\Component\Uid\Ulid;

test(
    'UlidGenerator constructs without factory and generate Id',
    function (): void {
        $ulidGenerator = new UlidGenerator;
        $em = createMock(EntityManager::class);
        $id = $ulidGenerator->generateId($em, null);
        expect($id)
            ->toBeInstanceOf(Ulid::class);
    },
);

test(
    'UlidGenerator constructs with factory and generate Id',
    function (): void {
        $factory = Mockery::mock(UlidFactory::class);
        $ulid = new Ulid;
        $factory->shouldReceive('create')
            ->andReturn($ulid)
            ->once();
        $ulidGenerator = new UlidGenerator($factory);
        $em = Mockery::mock(EntityManager::class);
        $id = $ulidGenerator->generateId($em, null);
        expect($id)
            ->toBe($ulid);
    },
);
