<?php declare(strict_types=1);

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\DBAL\Platforms\SQLitePlatform;
use Doctrine\DBAL\Types\Exception\InvalidType;
use Doctrine\DBAL\Types\Exception\ValueNotConvertible;
use Doctrine\DBAL\Types\Type;
use Lubiana\DoctrineUlid\Types\UlidType;
use Symfony\Component\Uid\Ulid;

it(
    'returns correct sql declaration on postgresql',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new PostgreSQLPlatform;

        // If platform supports guid type, declaration is guid type.
        $declaration = $ulidType->getSQLDeclaration([], $platform);
        expect($declaration)
            ->toBe('UUID');
    },
);

it(
    'returns correct sql declaration on sqlite',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new SqlitePlatform;

        // If platform supports guid type, declaration is guid type.
        $declaration = $ulidType->getSQLDeclaration([], $platform);
        expect($declaration)
            ->toBe('BLOB');
    },
);

it(
    'can convert string to a Ulid object',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new SQLitePlatform;
        $stringUlid = '01FGRM9X8ACP5D6HZG9K31D6CZ';
        $phpValue = $ulidType->convertToPHPValue($stringUlid, $platform);
        expect(
            $phpValue,
        )->toBeInstanceOf(Ulid::class)->and((string) $phpValue)->toBe($stringUlid);
    },
);

it(
    'throws if value is not string during conversion to PHP Value',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new SQLitePlatform;
        $nonStringValue = 123;
        expect(
            static fn(): mixed
                => $ulidType->convertToPHPValue($nonStringValue, $platform),
        )->toThrow(
            InvalidType::class,
        );
    },
);

it(
    'throws if value is not a valid Ulid string during conversion to PHP Value',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new SQLitePlatform;
        $invalidUlidString = 'invalid-ulid-string';
        expect(
            static fn(): mixed
                => $ulidType->convertToPHPValue($invalidUlidString, $platform),
        )->toThrow(
            ValueNotConvertible::class,
        );
    },
);

it(
    'can convert Ulid object to database value',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new SQLitePlatform;
        $ulidObject = new Ulid;
        $databaseValue = $ulidType->convertToDatabaseValue($ulidObject, $platform);
        expect($databaseValue)
            ->toBe($ulidObject->toBinary());
    },
);

it(
    'can convert Ulid string to a database value',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new SQLitePlatform;
        $ulidObject = new Ulid;
        $ulidBinary = $ulidObject->toBinary();
        $databaseValue = $ulidType->convertToDatabaseValue(
            (string) $ulidObject,
            $platform,
        );
        expect($databaseValue)
            ->toBe($ulidBinary);
    },
);

it(
    'will return null when value is null',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new SQLitePlatform;
        $nullValue = null;
        $databaseValue = $ulidType->convertToDatabaseValue($nullValue, $platform);
        expect($databaseValue)
            ->toBe(null);
    },
);

it(
    'throws if value is not string during conversion to Database Value',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new SQLitePlatform;
        $nonStringValue = 123;
        expect(
            static fn(): mixed
                => $ulidType->convertToDatabaseValue($nonStringValue, $platform),
        )->toThrow(
            InvalidType::class,
        );
    },
);

it(
    'throws if value is not a valid Ulid string during conversion to Database Value',
    function (): void {
        $ulidType = Type::getType(UlidType::NAME);
        $platform = new SQLitePlatform;
        $invalidUlidString = 'invalid-ulid-string';
        expect(
            static fn(): mixed
                => $ulidType->convertToDatabaseValue($invalidUlidString, $platform),
        )->toThrow(
            ValueNotConvertible::class,
        );
    },
);
