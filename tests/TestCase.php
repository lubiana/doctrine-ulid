<?php declare(strict_types=1);

namespace Tests;

use Lubiana\DoctrineUlid\Types\UlidType;
use Override;
use PHPUnit\Framework\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    #[Override]
    protected function setUp(): void
    {
        UlidType::register();
    }
}
