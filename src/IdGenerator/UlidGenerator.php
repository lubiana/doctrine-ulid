<?php declare(strict_types=1);

namespace Lubiana\DoctrineUlid\IdGenerator;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Override;
use Symfony\Component\Uid\Factory\UlidFactory;
use Symfony\Component\Uid\Ulid;

final class UlidGenerator extends AbstractIdGenerator
{
    public function __construct(
        private readonly UlidFactory|null $factory = null
    ) {}

    #[Override]
    public function generateId(EntityManagerInterface $em, object|null $entity): mixed
    {
        if ($this->factory instanceof UlidFactory) {
            return $this->factory->create();
        }

        return new Ulid;
    }
}
