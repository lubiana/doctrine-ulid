<?php declare(strict_types=1);

return [
    __DIR__ . '/src',
    __DIR__ . '/tests',
    __DIR__ . '/php-styler.php',
    __DIR__ . '/ecs.php',
    __DIR__ . '/rector.php',
];
